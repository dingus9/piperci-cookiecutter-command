# PiperCI Cookiecutter (Default)

## Table of Contents

* [Getting-Started](#getting-started)
* [Prerequisites](#prerequisites)
* [Usage](#usage)
* [Contributing](#contributing)
* [License](#license)

## Getting Started

This project defines a default project template for use with the PiperCI ecosystem.

## Prerequisites

Cookiecutter must be installed

```shell
pip install cookiecutter
```

## Usage

Invoke `cookiecutter` in the desired directory and answer the prompts

```shell
cookiecutter https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command.git
```

In order to create an echo faas repository, you can just use this command

```shell
cookiecutter --no-input https://gitlab.com/dreamer-labs/piperci/piperci-cookiecutter-command.git
```

In order to have tox unittests work correctly, you will have to initialize the git repository:

```shell
git init
```

If you are building a new command, you will need to manually modify the `supported_commands` files. Find the files:

```
find . -iname supported_commands
```

Include the actual commands that are required to be supported in these files.

## Contributing

Please read [CONTRIBUTING](https://piperci.dreamer-labs.net/project-info/contributing.html)
for details on our code of conduct, and the process for submitting merge requests to us.

## License

MIT
