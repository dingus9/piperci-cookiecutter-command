#!/bin/bash

FUNCTION_TEMPDIR=$1
FUNCTION_TEMPLATE=$2

THIS_SCRIPT=$(basename "${BASH_SOURCE[0]}")
THIS_RELDIR=$(dirname "${BASH_SOURCE[0]}")
REPO_BASEDIR=$(cd $THIS_RELDIR; cd ../../; pwd)

[[ -d ${FUNCTION_TEMPDIR} ]] && rm -r ${FUNCTION_TEMPDIR};

mkdir ${FUNCTION_TEMPDIR}

export REPO_BASEDIR
IFS=$'\n'
for function_data in $(python - <<DOC
import yaml
import os

def main():
    repo_dir = os.path.realpath(os.environ.get('REPO_BASEDIR', './'))
    stack_yml_path = f'{repo_dir}/stack.yml'
    with open(stack_yml_path, 'r') as stack_yml:
        stack = yaml.safe_load(stack_yml)
        for function, data in stack['functions'].items():
          print(f"{data['handler']} {data['lang']}")

if __name__ == '__main__':
  main()

DOC);
do
  function_path="${function_data% *}"
  function_name=$(basename $function_path)
  function_lang="${function_data#* }"

  cp -r ${FUNCTION_TEMPLATE}/${function_lang} ${FUNCTION_TEMPDIR}/${function_name}
  cp -r ${function_path}/*.py ${FUNCTION_TEMPDIR}/${function_name}/function
  cp ${FUNCTION_TEMPLATE}/${function_lang}/faas_app.py ${FUNCTION_TEMPDIR}/${function_name}/function/faas_app.py
  cp -r ${function_path}/config.py ${FUNCTION_TEMPDIR}/${function_name}/config.py
  cp ${function_path}/supported_commands ${FUNCTION_TEMPDIR}/${function_name}/function/
  cp ${function_path}/supported_commands ${FUNCTION_TEMPDIR}/${function_name}/


done

cp -r ${REPO_BASEDIR}/tests ${FUNCTION_TEMPDIR}/tests
touch ${FUNCTION_TEMPDIR}/__init__.py
